# QF-Test Gradle Plugin
This Plugin contains the QF-Test gradle plugin, to automatically integrate QF-Test into your Gradle projects.
For this purpose, you can both integrate a local QF-Test or have a specific version downloaded. 
Limited support is available with QF-Test 6.0.4, but for most features you need at least QF-Test 6.0.5.

## How to Build Locally
### Compile QF-Test Gradle Plugin
The current folder is an IntelliJ IDEA Gradle Project.
Clone the build environment into the desired (user) directory.
### Deploy to Local Maven Repository
```bash
cd PluginSource # (go to the project root)
./gradlew clean publishToMavenLocal
```
### Integrate the Local QF-Test Gradle Plugin Into Your Project
- Go to `settings.gradle` in your project
- Add the integration with the maven local repository:
```groovy
pluginManagement {
    repositories {
        mavenLocal()
        gradlePluginPortal()
    }
}
```
- Go to `build.gradle` in your project
- Add the integration of the QF-Test plugin with the latest version
```groovy
 plugins {
 	id 'de.qfs.qftest' version '2.1.0-SNAPSHOT'
 }
```
## How to Integrate QF-Test Into Your Project
Add the plugin to your project's `build.gradle` using:
```groovy
 plugins {
 	id 'de.qfs.qftest' version '2.1.0'
 }
```

## License

This plugin source is avaible under the terms of the MIT License (MIT), as described in `LICENSE`