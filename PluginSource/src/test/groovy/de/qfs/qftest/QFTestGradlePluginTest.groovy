package de.qfs.qftest

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertNotNull
import static org.junit.jupiter.api.Assertions.assertNull
import static org.junit.jupiter.api.Assertions.assertTrue

@DisplayName("QFTest Gradle Plugin Test")
class QFTestGradlePluginTest {

    Project project

    @BeforeEach
    void setUp() {
        project = ProjectBuilder.builder().build()
        QFTestGradlePlugin plugin = new QFTestGradlePlugin()
        plugin.apply(project)
    }

    @Test
    void testQFTestExtensionAdded() {
        assertNotNull( project.getExtensions().getByType(QFTestGradleExtension.class), "QFTestGradleExtension not added")
    }

    @Test
    void testStartQFTestTaskAdded() {
        assertNotNull(project.getTasks().named("startQFTest"), "StartQFTest task not added")
    }

    @Test
    void testGetFilenameOfClass() {
        String fileName = QFTestGradlePlugin.getFilenameOfClass(this.getClass())
        String expected = new File(System.getProperty("user.dir"),"build/classes/groovy/test").getAbsolutePath()
        assertEquals(expected, fileName, "Filename end with QFTestGradlePluginTest.class")
    }

    @Test
    void testExtractShadowFileFromZip() {
        final String TEST_DIR = "src/test/resources"
        final String ZIP_FILE_NAME = "test-data.zip"
        final String TARGET_FILE_NAME = "exampleFile.txt"
        final String DESTINATION_DIR = "build/extracted"

        QFTestGradlePlugin.extractShadowFileFromZip(Paths.get(TEST_DIR, ZIP_FILE_NAME).toString(), TARGET_FILE_NAME, DESTINATION_DIR)
        Path extractedFile = Paths.get(DESTINATION_DIR, TARGET_FILE_NAME)

        try {
            assertTrue(Files.exists(extractedFile), "Extracted file should exist")
            assertTrue(Files.isRegularFile(extractedFile), "Extracted file should be a regular file")
            assertEquals("Example Text.",extractedFile.toFile().text, "Extracted file should contain Example Text.")
        } finally {
            try {
                Files.delete(extractedFile)
            } catch (Exception e) {
                e.printStackTrace()
            }
        }
    }

    @Test
    void testFindSuites() {

        List<String> suites = ["src/test/resources/suite1", "src/test/resources/suite2"]
        List<String> foundSuites = QFTestGradlePlugin.findSuites(suites, project)
        foundSuites.eachWithIndex { it, index ->
            foundSuites[index] = it.replace('\\', '/')
        }

        assertNotNull(foundSuites, "Found suites should not be null")
        assertEquals(4, foundSuites.size(), "Found suites should contain 4 qft files")
        assertTrue(foundSuites.contains("src/test/resources/suite1/test1.qft"), "Found suites should contain test1.qft")
        assertTrue(foundSuites.contains("src/test/resources/suite1/test2.qft"), "Found suites should contain test2.qft")
        assertTrue(foundSuites.contains("src/test/resources/suite2/test3.qft"), "Found suites should contain test3.qft")
        assertTrue(foundSuites.contains("src/test/resources/suite2/test4.qft"), "Found suites should contain test4.qft")
    }

    @Test
    void testFindSuites_DirectoryInput() {

        String inputPath = "src/test/resources/suite1"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        qftFiles.eachWithIndex { it, index ->
            qftFiles[index] = it.replace('\\', '/')
        }

        assertNotNull(qftFiles, "Result should not be null")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test1.qft"), "Result should contain test1.qft")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test2.qft"), "Result should contain test2.qft")
    }

    @Test
    void testFindSuites_FileInput() {

        String inputPath = "src/test/resources/suite1/test1.qft"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        assertNotNull(qftFiles, "Result should not be null")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test1.qft"), "Result should contain test1.qft")
    }

    @Test
    void testFindSuites_PatternInput() {

        String inputPath = "src/test/resources/**/*.qft"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        qftFiles.eachWithIndex { it, index ->
            qftFiles[index] = it.replace('\\', '/')
        }

        assertNotNull(qftFiles, "Result should not be null")
        println qftFiles
        assertTrue(qftFiles.contains(new File("src/test/resources/suite1/test1.qft").absolutePath), "Result should contain test1.qft")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite1/test2.qft").absolutePath), "Result should contain test2.qft")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite2/test3.qft").absolutePath), "Found suites should contain test3.qft")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite2/test4.qft").absolutePath), "Found suites should contain test4.qft")
    }

    @Test
    void testFindSuites_DirectoryInputWithTestcaseName() {

        String inputPath = "src/test/resources/suite1#TC"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        qftFiles.eachWithIndex { it, index ->
            qftFiles[index] = it.replace('\\', '/')
        }

        assertNotNull(qftFiles, "Result should not be null")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test1.qft#TC"), "Result should contain test1.qft#TC")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test2.qft#TC"), "Result should contain test2.qft#TC")
    }

    @Test
    void testFindSuites_FileInputWithTestcaseName() {

        String inputPath = "src/test/resources/suite1/test1.qft#TestcaseName"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        assertNotNull(qftFiles, "Result should not be null")
        assertTrue(qftFiles.contains("src/test/resources/suite1/test1.qft#TestcaseName"), "Result should contain test1.qft#TestcaseName")
    }

    @Test
    void testFindSuites_PatternInputWithTestcaseName() {

        String inputPath = "src/test/resources/**/*.qft#Test"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        qftFiles.eachWithIndex { it, index ->
            qftFiles[index] = it.replace('\\', '/')
        }

        assertNotNull(qftFiles, "Result should not be null")
        println qftFiles
        assertTrue(qftFiles.contains(new File("src/test/resources/suite1/test1.qft#Test").absolutePath), "Result should contain test1.qft#Test")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite1/test2.qft#Test").absolutePath), "Result should contain test2.qft#Test")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite2/test3.qft#Test").absolutePath), "Found suites should contain test3.qft#Test")
        assertTrue(qftFiles.contains(new File("src/test/resources/suite2/test4.qft#Test").absolutePath), "Found suites should contain test4.qft#Test")
    }


    @Test
    void testFindSuites_NonexistentInput() {
        String inputPath = "nonexistent/path"
        List<String> qftFiles = QFTestGradlePlugin.findSuites([inputPath], project)
        assertNotNull(qftFiles, "Result should not be null")
        assertEquals(0, qftFiles.size(), "Result should be empty for nonexistent input")
    }

    @Test
    void testSeparatePathAndPattern_ValidInput() {
        def inputString = "path/to/files/*.txt"
        def result = QFTestGradlePlugin.separatePathAndPattern(inputString)
        assertNotNull(result, "Result should not be null")
        assertEquals("path/to/files/", result[0], "Path should be 'path/to/files/'")
        assertEquals("*.txt", result[1], "Pattern should be '*.txt'")
    }

    @Test
    void testSeparatePathAndPattern_NoPattern() {
        def inputString = "path/to/files/"
        def result = QFTestGradlePlugin.separatePathAndPattern(inputString)
        assertNotNull(result, "Result should not be null")
        assertEquals("path/to/files/", result[0], "Path should be 'path/to/files/'")
        assertNull(result[1], "Pattern should be null")
    }

    @Test
    void testSeparatePathAndPattern_NullInput() {
        def result = QFTestGradlePlugin.separatePathAndPattern(null)
        def resultList = ["./", "null"]
        assertEquals(resultList[0], result[0], "Result should be ./")
        assertEquals(resultList[1], result[1], "Result should be null")
    }

    @Test
    void testLocateLatestQFTestDir() {
        ArrayList<String> qfTestPaths = new ArrayList<>()
        qfTestPaths.add("/some/path/qftest-6.0.2")
        qfTestPaths.add("/some/path/qftest-6.1.0")
        qfTestPaths.add("/some/path/qftest-6.1.11")
        qfTestPaths.add("/some/path/qftest-6.0.4")
        qfTestPaths.add("/some/path/qftest-6.1.4")

        QFTestGradleExtension extension = project.getExtensions().getByType(QFTestGradleExtension.class)
        QFTestGradlePlugin.locateLatestQFTestDir(qfTestPaths, extension)

        assertEquals("/some/path/qftest-6.1.11", extension.getVersionDir(), "Latest QFT-Test should be /some/path/qftest-6.1.11")
    }

    @Test
    void testLocateLatestQFTestDirWithDev() {
        ArrayList<String> qfTestPaths = new ArrayList<>()
        qfTestPaths.add("/some/path/qftest-6.0.2")
        qfTestPaths.add("/some/path/qftest-7.0.0-xyz")
        qfTestPaths.add("/some/path/qftest-6.1.11")

        QFTestGradleExtension extension = project.getExtensions().getByType(QFTestGradleExtension.class)
        QFTestGradlePlugin.locateLatestQFTestDir(qfTestPaths, extension)

        assertEquals("/some/path/qftest-7.0.0-xyz", extension.getVersionDir(), "Latest QFT-Test should be /some/path/qftest-7.0.0-xyz")
    }

    @Test
    void testLocateLatestQFTestDirWithDevNotHighest() {
        ArrayList<String> qfTestPaths = new ArrayList<>()
        qfTestPaths.add("/some/path/qftest-6.0.2")
        qfTestPaths.add("/some/path/qftest-6.1.0-xyz")
        qfTestPaths.add("/some/path/qftest-6.1.11")

        QFTestGradleExtension extension = project.getExtensions().getByType(QFTestGradleExtension.class)
        QFTestGradlePlugin.locateLatestQFTestDir(qfTestPaths, extension)

        assertEquals("/some/path/qftest-6.1.11", extension.getVersionDir(), "Latest QFT-Test should be /some/path/qftest-6.1.11")
    }
}