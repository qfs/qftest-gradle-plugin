// Copyright (c) 2023 Quality First Software GmbH
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.

package de.qfs.qftest
import org.gradle.api.Project

class QFTestGradleExtension {

    String license
    String versionDir
    String runLogDir
    String reportDir
    String version
    String cacheDir
    def arguments
    def options
    def variables
    def keepRunLogs
    def reportArgs
    String pluginClass
    def devScopes

    // If "version" specifies a dev-version, the concrete build version must be specified here.
    // Dev-versions are only available for a limited, time, so this should not be used in production.
    // Alternatively, baseUrl can be specified
    def build

    // Base URL for fetching QF-Test distributions
    String baseUrl = "https://www.qfs.de/fileadmin/Webdata/pub/qftest"

    QFTestGradleExtension(Project project) {

        this.cacheDir = project.getGradle().getStartParameter().getProjectCacheDir()
        if (! this.cacheDir) {
            this.cacheDir = project.layout.projectDirectory.dir(".gradle").getAsFile().getAbsolutePath()
        }

        new File(this.cacheDir).mkdirs() // make sure cacheDir exists

    }

    static def create(Project project) {
        return project.extensions.create("qftest", QFTestGradleExtension.class, project)
    }

    List<File> resolveLibs(libs) {
        if (libs == null) {
            libs = []
        }
        libs = asList(libs)
        libs.addAll(["qflib", "qfshared"])

        String versionDir = this.versionDir

        def qftestVersionDir = new File(versionDir as String)
        List<File> qftestJars = libs
                .collectMany { p -> [
                        new File(qftestVersionDir, "qflib/" + (p.startsWith("qf") ? p : "qf" + p) + ".jar"),
                        new File(qftestVersionDir, "lib/" + p + ".jar")
                    ]
                }
               .findAll { f -> f.exists()}
        return qftestJars
    }

    static def asList(list) {
        if (list == null) return null;
        if (list instanceof List) return list;
        return [list];
    }
}