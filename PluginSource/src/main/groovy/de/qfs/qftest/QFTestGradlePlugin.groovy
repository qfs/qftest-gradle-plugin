// Copyright (c) 2023 Quality First Software GmbH
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.

package de.qfs.qftest

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.testing.Test
import org.apache.tools.ant.taskdefs.condition.Os
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.zip.ZipFile


class QFTestGradlePlugin implements Plugin<Project> {

    static def logger = LoggerFactory.getLogger("qftest-plugin")
    public static final String RUN_TESTSUITE_CLASS = "de/qfs/qftest/RunQFTestSuites.class"
    public static final String RUN_TESTSUITE_CLASSPATH = "qftest-helpers/testRunClasspath"

    def testClassFileCollection

    static String getFilenameOfClass( final Class<?> theClass) {
        try {
            String uri = theClass.getProtectionDomain().getCodeSource().getLocation().toURI().toString()
            uri = uri.replaceFirst("^file://","file:////") // java calculates unc path wrongly in URI https://stackoverflow.com/questions/1878024/file-uris-and-slashes
            final String fileName = new File(new java.net.URI(uri)).getAbsolutePath()
            return fileName
        } catch (final Exception e) {
            e.printStackTrace()
        }
        return null
    }

    static void extractShadowFileFromZip(String zipFilePath, String targetFileName, String destinationDir) {
        ZipFile zipFile = new ZipFile(zipFilePath)

        zipFile.entries().each { entry ->
            if (entry.name == targetFileName+ ".shadow") {
                Path destinationPath = Paths.get(destinationDir, targetFileName)
                destinationPath.toFile().getParentFile().mkdirs()
                Files.copy(zipFile.getInputStream(entry), destinationPath, StandardCopyOption.REPLACE_EXISTING)
            }
        }

        zipFile.close()
    }

     FileCollection getTestClassFileCollection (Project project) {
         if (testClassFileCollection){
             return testClassFileCollection
         }

         def extension = project.extensions.getByType(QFTestGradleExtension.class)
         def zipFilePath = getFilenameOfClass(QFTestGradlePlugin.class)
         logger.debug("extensionSource: " + zipFilePath)
         def targetFileName = RUN_TESTSUITE_CLASS
         def destinationDir = new File(extension.cacheDir, RUN_TESTSUITE_CLASSPATH).absolutePath

         extractShadowFileFromZip(zipFilePath, targetFileName, destinationDir)

         testClassFileCollection = project.files(destinationDir)

         return testClassFileCollection
    }

    static List<String> findSuites (suites, project){
        def suiteList = suites.collect { path ->

                if (! path) return null

                def hashIndex = path.lastIndexOf("#")
                def suffix = ""
                if (hashIndex > -1) {
                    suffix = path.substring(hashIndex)
                    path = path.substring(0,hashIndex)
                }

                path = path.replaceAll(/[\\\/]+/, '/')

                def qftFiles = findQftFiles (path,suffix,project)
                return qftFiles
            }.flatten().findAll { it != null }
        return suiteList as List<String>
    }

    static List<String> findQftFiles (inputPath,suffix,project) {
        def fileOrDir = new File(inputPath)

        if (fileOrDir.exists()) {
            if (fileOrDir.isDirectory()) {
                def qftFiles = fileOrDir.listFiles({ File file -> file.name.endsWith('.qft') } as FileFilter)
                fileOrDir.eachDir { subDir ->
                    qftFiles += findQftFiles(subDir.absolutePath,suffix,project)
                }
                return qftFiles.collect { it.toString() + suffix }
            } else {
                return [inputPath + suffix]
            }
        } else {
            def pathAndPattern = separatePathAndPattern(inputPath)
            def fileTree = project.fileTree(dir: new File(pathAndPattern[0]).absolutePath, include: pathAndPattern[1])
            def fileList = fileTree.files.collect { it.toString() + suffix }
            return fileList
        }
    }

    static def separatePathAndPattern(String inputString) {

        def pattern = /^([^*?{}\[\]!+@]+\/)?(.+)?$/
        def matcher = inputString =~ pattern

        if (matcher.matches()) {

            String path = matcher[0][1] ?: "./"
            String globPattern = matcher[0][2]

            logger.debug "current inputString: $inputString"
            logger.debug "pathPart: $path"
            logger.debug "globPart: $globPattern"
            return [path, globPattern]
        } else {
            logger.error "The path $inputString could not be found"
            return null
        }
    }

    @Override
    void apply(Project project) {

        QFTestGradleExtension.create(project)

        def extension = project.extensions.getByType(QFTestGradleExtension.class)

        project.tasks.withType(Test).configureEach { task ->
            task.extensions.create('qftest', QFTestTaskExtension)
        }

        project.extensions.extraProperties.set("StartQFTestTask", StartQFTestTask.class)

        project.tasks.register("startQFTest", StartQFTestTask.class)

        project.afterEvaluate {

            logger.debug "VersionDir: " + extension.versionDir
            logger.debug "RunLogDir: " + extension.runLogDir
            logger.debug "ReportDir: " + extension.reportDir
            logger.debug "Version: " + extension.version

            if (!extension.versionDir) {

                logger.info "No versionDir specified"

                if ((extension.version == "local") || (extension.version == "installed")) {

                    locateQFTest(extension)

                } else {

                    downloadQFTest(extension)
                }
            }

            def pluginClass = extension.pluginClass
            def devScopes = QFTestGradleExtension.asList(extension.devScopes)

            if (pluginClass?.length() > 0 || devScopes?.size() > 0) {
                if (project.configurations.hasProperty("implementation")) {
                    def resolvedLibs = extension.resolveLibs(devScopes)
                    logger.info "QF-Test implementation Jars: " + resolvedLibs
                    project.configurations.implementation.dependencies.add(
                            project.dependencies.add("implementation", project.files(resolvedLibs))
                    )
                }

                if (pluginClass?.length() > 0) {
                    project.getTasksByName("classes", false).each { task ->
                        task.doLast({
                            def resourcesDir = project.sourceSets.main.output.resourcesDir
                            def servicesDir = new File(resourcesDir, "META-INF/services")
                            if (servicesDir.isDirectory() || servicesDir.mkdirs()) {
                                def serviceFile = new File(servicesDir, "de.qfs.apps.qftest.shared.QFTestPlugin")
                                serviceFile.text = pluginClass
                                logger.debug("Created service file: " + serviceFile)
                            }
                        })
                    }
                }
            }

            if (project.configurations.hasProperty("testImplementation")) {
                def resolvedLibs = extension.resolveLibs(["qftest", "truezip"])
                logger.info "QF-Test testImplementation jars: " + resolvedLibs
                project.configurations.testImplementation.dependencies.add(
                        project.dependencies.add("testImplementation", project.files(resolvedLibs))
                )
            }

            project.getAllTasks(true).values().each {projectTasks ->
                projectTasks.each {task ->
                    if (task instanceof StartQFTestTask) {
                        if (pluginClass?.length() > 0 || devScopes?.size() > 0) {
                            task.dependsOn("classes")
                        }
                    } else if (task instanceof Test) {

                        def qfTestExtension = task.extensions.getByType(QFTestTaskExtension.class)

                        task.systemProperty("qftest.versiondir", extension.versionDir)

                        logger.debug ("withReportOpen: " + qfTestExtension.withReportOpen)

                        qfTestExtension.suites = QFTestGradleExtension.asList(qfTestExtension.suites)

                        def suites = qfTestExtension.suites

                        if (suites) {

                            logger.debug "inputSuites: " + suites
                            suites = findSuites(suites, project)
                            logger.debug "runningSuites: " + suites

                            if (suites.isEmpty()) {
                                logger.warn("No suites found for definition: " + qfTestExtension.suites)
                            } else {
                                def testClassFileCollection = getTestClassFileCollection(project)

                                task.testClassesDirs += testClassFileCollection
                                task.classpath += testClassFileCollection

                                logger.debug(task.name + " testClassesDirs: " + task.testClassesDirs.getFiles())
                                logger.debug(task.name + " classpath: " + task.classpath.getFiles())

                                task.systemProperty("qftest.suites", suites.join(File.pathSeparator))
                                qfTestExtension.suites = suites

                                // Force rerun since suites might produce a different result due to external factors
                                task.getOutputs().upToDateWhen { false }
                            }
                        }

                        if (qfTestExtension.withReportOpen) {
                            task.systemProperty("qftest.withreportopen", qfTestExtension.withReportOpen)
                        }

                        if (extension.license) {
                            task.systemProperty("qftest.license", wrapForSystemProperty(extension.license))
                        }
                        if (extension.runLogDir) {
                            task.systemProperty("qftest.runlogdir", extension.runLogDir)
                        }
                        if (extension.reportDir) {
                            task.systemProperty("qftest.withreportindir", extension.reportDir)
                        }

                        if (extension.arguments) {
                            task.systemProperty("qftest.arguments", wrapForSystemProperty(extension.arguments))
                        }
                        if (extension.options) {
                            task.systemProperty("qftest.options", wrapForSystemProperty(extension.options).toString())
                        }
                        if (extension.variables) {
                            task.systemProperty("qftest.variables", wrapForSystemProperty(extension.variables).toString())
                        }
                        if (extension.keepRunLogs) {
                            task.systemProperty("qftest.keeprunlogs", wrapForSystemProperty(extension.keepRunLogs).toString())
                        }
                        if (extension.reportArgs) {
                            task.systemProperty("qftest.reportargs", wrapForSystemProperty(extension.reportArgs).toString())
                        }
                        logger.debug("Extra Test System-Properties:" + task.systemProperties)
                    }
                }
            }

        }
    }

    private String wrapForSystemProperty(value) {
        def wrapped = JsonOutput.toJson(value).toString()

        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            // Work around Windows Gradle bug: https://github.com/gradle/gradle/issues/6072
            logger.debug("@@@ wrapForSystemProperty = " + wrapped)
            wrapped = wrapped.replaceAll("\"", "\\\\\"")
            wrapped = wrapped.replaceAll(" ", "\" \"")
            wrapped = "\"" + wrapped + "\""
            logger.debug("@@@ wrapForSystemPropertyWrapped = " + wrapped)
            return wrapped
        }
        return wrapped
    }

    static def locateQFTest(QFTestGradleExtension extension) {

        logger.debug "checking local files"

        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            locateQFTestOnWindows(extension)
        } else if (Os.isFamily(Os.FAMILY_MAC)) {
            locateQFTestOnMacOs(extension)
        }

        if (!extension.versionDir) {
            locateQFTestOnLinux(extension)
        }

        if (!extension.versionDir) {
            logger.error "No local QF-Test found"
            logger.error "Please set the configuration parameter \"versionDir\" explicitly."
            throw new Exception("No local QF-Test found\n Please set the configuration parameter \"versionDir\" explicitly.")
        }
    }

    private static void locateQFTestOnWindows(QFTestGradleExtension extension) {
        def filePaths = new HashSet([System.getenv("ProgramFiles"), System.getenv("ProgramW6432"), System.getenv("ProgramFiles(X86)")])
        def innerFilePath = "QFS"
        def fileOuterPrefix = "QF-Test"
        def qfTestPaths = new ArrayList<String>()

        for (filePath in filePaths) {
            logger.debug "Searching for QF-Test in filePath: " + filePath
            def rootDir = new File("$filePath/$innerFilePath/$fileOuterPrefix")
            logger.debug "rootDir: " + rootDir.absolutePath

            addQFTestsFromDirToPaths(rootDir, qfTestPaths)
        }

        locateLatestQFTestDir(qfTestPaths, extension)
    }

    private static void addQFTestsFromDirToPaths(File rootDir, qfTestPaths) {
        def filePrefix = "qftest-"
        if (rootDir.exists()) {
            logger.debug "rootDir exists"
            def dir = new File(rootDir.absolutePath)
            logger.debug "dir: " + dir
            def matchingFiles = dir.listFiles({ filedir, name -> name.startsWith(filePrefix) } as FilenameFilter)
            logger.debug "matchingFiles: " + matchingFiles
            if (matchingFiles.length > 0) {
                logger.debug("Found matching files in directory $rootDir:")
                matchingFiles.each { file -> qfTestPaths.add(file.absolutePath) }
            } else {
                logger.debug("No files found with prefix $filePrefix in directory $rootDir")
            }
        }
    }

    static void locateLatestQFTestDir(ArrayList<String> qfTestPaths, QFTestGradleExtension extension) {

        if (qfTestPaths.size() > 0) {
            def qfTestPathsWithVersion = qfTestPaths.findAll { it =~ /qftest-\d+\.\d+\.\d+(-.*)?$/ }
            if (qfTestPathsWithVersion.isEmpty()) {
                return
            }
            def qftestVersions = qfTestPathsWithVersion.collect { path ->
                def versionString = path.tokenize("/\\")[-1].tokenize("-")[1]
                versionString.tokenize(".").collect { it.toInteger() }
            }
            def versionsIntSorted = qftestVersions.sort { a, b ->
                for (i in 0..<a.size()) {
                    if (a[i] != b[i]) {
                        return a[i] <=> b[i]
                    }
                }
            }

            def latestVersion = "qftest-" + versionsIntSorted.last().join('.')

            extension.versionDir = qfTestPathsWithVersion.find { it.contains(latestVersion) }
            logger.debug "New local VersionDir: " + extension.versionDir
        }
    }

    private static void locateQFTestOnMacOs(QFTestGradleExtension extension) {
        def globalQFTest = new File("/Applications/QF-Test.app/Contents/Resources/qftest/qftest-current/")
        if (globalQFTest.exists()) {
            extension.versionDir = globalQFTest.absolutePath
            logger.debug "New local VersionDir: " + extension.versionDir
            return
        }

        def localQFTest = new File(System.getProperty("user.home") + "/Applications/QF-Test.app/Contents/Resources/qftest/qftest-current/")
        if (localQFTest.exists()) {
            extension.versionDir = localQFTest.absolutePath
            logger.debug "New local VersionDir: " + extension.versionDir
        }
    }

    private static void locateQFTestOnLinux(QFTestGradleExtension extension) {

        def rootPath = System.getProperty("user.home") + "/qftest"

        def rootDir = new File(rootPath)
        logger.debug "rootDir: " + rootDir.absolutePath

        def qfTestPaths = new ArrayList<String>()

        addQFTestsFromDirToPaths(rootDir, qfTestPaths)
        locateLatestQFTestDir(qfTestPaths, extension)
    }

    static def downloadQFTest(QFTestGradleExtension extension) {

        logger.info "Settings for download: CacheDir: " + extension.cacheDir + " Version: " + extension.version


        if ((!extension.version) || (extension.version == "latest")) {

            String versionUrl = getBaseUrl(extension) + "/VersionInfo.json"

            logger.info "Getting version information from: " + versionUrl
            try {
                def json = new JsonSlurper().parse(new URL(versionUrl))
                extension.version = json.version.get(0) + json.suffix.get(0)
            } catch (Exception ex) {
                logger.error "Cant get latest Version: " + ex
                logger.error "1. check the internet connection"
                logger.error "2. try to set a specific version like: 'version = 6.0.4'"

                throw ex
            }
            logger.info "Current Version: " + extension.version
        }

        File versionDirFile = new File(extension.cacheDir, "qftest/qftest-${extension.version}")

        if (Os.isFamily(Os.FAMILY_MAC)) {
            versionDirFile = new File(extension.cacheDir, "QF-Test-${extension.version}.app/Contents/Resources/qftest/qftest-current")
        }

        logger.debug "VersionDirFile already exists: " + versionDirFile.parentFile.exists()
        logger.debug "VersionDirFile: " + versionDirFile

        if(!versionDirFile.exists()) {
            download(extension)
        }
        extension.versionDir = versionDirFile.absolutePath
        logger.debug "New VersionDir: " + extension.versionDir
    }

    static def download(QFTestGradleExtension extension) {

        def fileName = "QF-Test-${extension.version}-sfx.exe"

        if (Os.isFamily(Os.FAMILY_MAC)) {
            fileName = "QF-Test-${extension.version}.dmg"
        } else if (Os.isFamily(Os.FAMILY_UNIX)) {
            fileName = "QF-Test-${extension.version}.tar.gz"
        }

        def sourceUrl = getBaseUrl(extension) + "/" + fileName

        def downloadedFile = new File(extension.cacheDir, fileName)
        logger.debug "Downloaded file: " + downloadedFile

        logger.lifecycle "Downloading from: " + sourceUrl + " ..."
        def downloadException = null
        try {
            downloadedFile.withOutputStream { out ->
                new URL(sourceUrl).withInputStream { from -> out << from }
            }
            logger.lifecycle "done"
        } catch(Exception ex) {
            downloadException = ex
        }

        if (! downloadedFile.exists() || downloadException) {
            logger.error "Download failed." + (downloadException ? ": " + downloadException : "")
            logger.error "1. check the internet connection"
            logger.error "2. check if the URL ${sourceUrl} is reacheable"
            logger.error "3. check if the version ${extension.version} is supported"
            if (downloadException) {
                throw downloadException
            }
        }

        logger.lifecycle "Extracting $downloadedFile ..."
        try {
            if (Os.isFamily(Os.FAMILY_WINDOWS)) {
                def run = new ProcessBuilder(downloadedFile.absolutePath,"-y").directory(new File(extension.cacheDir)).start()
                run.waitForProcessOutput(System.out, System.err)
            } else if (Os.isFamily(Os.FAMILY_MAC)) {
                extractFromMacOsDiskImage(downloadedFile, extension)

            } else if (Os.isFamily(Os.FAMILY_UNIX)) {
                def run = new ProcessBuilder("tar", "-xzf", downloadedFile.absolutePath).directory(new File(extension.cacheDir)).start()
                run.waitForProcessOutput(System.out, System.err)
            }
            logger.lifecycle "done"
        } catch(Exception ex) {
            logger.error "Extracting failed: " + ex
        }
        downloadedFile.delete()
    }

    private static def getBaseUrl(extension) {
        def baseUrl = extension.baseUrl
        if (extension.build) {
            if (baseUrl.endsWith("/qftest")) {
                baseUrl = baseUrl.substring(0,baseUrl.length() - 6) + "dev/" + extension.build
            }
        }
        return baseUrl
    }

    private static void extractFromMacOsDiskImage(File inputFile, QFTestGradleExtension extension) {
        def runMount = new ProcessBuilder("/usr/bin/hdiutil", "attach", inputFile.absolutePath).start()
        def outputMount = runMount.getText()
        def matcher = outputMount =~ /(?m)^(\/dev\/\S+)\s+Apple_HFS\s+(\/Volumes\/QF-Test ${extension.version})/

        if (!matcher) {
            logger.error outputMount
            throw new Exception("Could not mount " + inputFile)
        }
        logger.info outputMount

        String disk = matcher[0][1]
        String mountpoint = matcher[0][2]

        def sourcePath = mountpoint + "/QF-Test.app"
        def targetPath = extension.cacheDir + "/QF-Test-${extension.version}.app"

        Files.walk(Paths.get(sourcePath)).forEach {
            def dest = Paths.get(targetPath, it.toString().substring(sourcePath.length()))
            logger.trace "Copying ${it.toString()} tp ${dest}"
            try {
                Files.copy(it, dest, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES)
            } catch (e) {
                logger.error "Could not copy ${it.toString()}: " + e
            }
        }

        def runUnmount = new ProcessBuilder("/usr/bin/hdiutil", "detach", disk).start()
        def outputUnmount = runUnmount.getText()
        logger.info outputUnmount
    }

}