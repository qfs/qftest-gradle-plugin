package de.qfs.qftest

import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

abstract class StartQFTestTask extends DefaultTask {

    @Input
    def arguments = new ArrayList()

    @Input
    boolean fork = true

    public static File getQFTestBinary(String versionDir, boolean extendClasspath)
    {
        if (versionDir == null) return null

        final File bindir = new File(versionDir,"bin")

        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            return new File(bindir, "qftestc.exe")
        } else if (Os.isFamily(Os.FAMILY_MAC) && ! extendClasspath) { // "QFTEST_CLASSPATH" not supported by mac starter
            final File macBinary
            try {
                macBinary = new File(bindir,"../../../../MacOS/QF-Test").getCanonicalFile()
                if (macBinary.exists()) {
                    return macBinary
                }
            } catch (IOException e) {
                // ignore, fall back to default
            }
        }
        return new File(bindir, "qftest")
    }

    @TaskAction
    void startQFTest() {
        def project = this.getProject()
        def extension = project.extensions.getByType(QFTestGradleExtension)
        if (! extension.versionDir) {
            throw new Exception("Could not locate QF-Test.")
        }
        def pluginClass = extension.pluginClass
        def devScopes = QFTestGradleExtension.asList(extension.devScopes)
        def extendClasspath = pluginClass?.length() > 0 || devScopes?.size() > 0


        arguments.add(0, getQFTestBinary(extension.versionDir,extendClasspath).absolutePath)

        logger.lifecycle "Starting QF-Test" + (extension.version ? " (${extension.version})" : "")
        logger.info ("arguments", arguments)
        try {
            def builder = new ProcessBuilder(arguments)

            if (extendClasspath) {
                def paths = []
                for (def dir: project.sourceSets.main.output) {
                    paths.add(dir.absolutePath)
                }

                def extraClasspath = paths.join(File.pathSeparator)
                logger.info ("extraClasspath",extraClasspath)
                builder.environment().put("QFTEST_CLASSPATH",extraClasspath)
            }

            def run = builder.start()
            if (fork) {
                run.consumeProcessOutput()
            } else {
                run.consumeProcessOutputStream(System.out)
                run.consumeProcessErrorStream(System.err)
                run.waitFor()
            }

        } catch(Exception ex){
            logger.error "QF-Test start failed: " + ex
        }
    }
}
