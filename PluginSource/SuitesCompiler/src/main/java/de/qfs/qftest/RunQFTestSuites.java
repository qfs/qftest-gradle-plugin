// Copyright (c) 2023 Quality First Software GmbH
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.

package de.qfs.qftest;

import de.qfs.apps.qftest.junit5.QFTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;

import java.io.File;

public class RunQFTestSuites {

    String[] suites = System.getProperty("qftest.suites", "").split(File.pathSeparator);

    boolean withReportOpen = Boolean.parseBoolean(System.getProperty("qftest.withreportopen", ""));

    boolean suitesDefined() {
        return suites.length > 0;
    }
    @EnabledIf("suitesDefined")
    @QFTest.Test
    QFTest runQftestSuites() throws Exception {
        if (withReportOpen) {
            return QFTest.runSuites(suites).withReportOpen();
        } else {
            return QFTest.runSuites(suites);
        }
    }
}
