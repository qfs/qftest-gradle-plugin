# QF-Test Gradle Plugin
This Plugin contains the QF-Test gradle plugin, to automatically integrate QF-Test into your Gradle projects.
For this purpose, you can both integrate a local QF-Test or have a specific version downloaded. 
Required is QF-Test 6.0.5 and above.


`DemoProject` 
	: An example project of how to integrate and work with the QF-Test Gradle Plugin. 

`PluginSource`
	:  The sourcecode of the QF-Test Gradle Plugin.

Each of those subfolders is a separate IntelliJ project.

## How to Integrate QF-Test Into Your Project
Add the plugin to your project's `build.gradle` using:
```groovy
 plugins {
 	id 'de.qfs.qftest' version '2.1.0'
 }
```

*Extensive example and README can be found in the `DemoProject` folder.*

***
### Release Notes

#### 2.1.0
##### New Features
- Support for QF-Test plugin development using the configuration parameters `pluginClass` and `devScopes`.

#### 2.0.2
###### Bugfixes
- Fixed NullPointerException when no test task is configured
- Allow specification of test cases in suite lists
- Fixed special argument passing on Windows

#### 2.0.1
###### Bugfixes
- Fixed a bug where runLogDir, keepRunLogs, reportDir and reportArgs were not passed properly to QF-Test
- Fixed a bug where percent symbol were not passed properly to QF-Test on windows
- Does not crash task execution if default "test" task is not configured

#### 2.0.0
##### New Features
- Test suites can now be executed from the _test_ task without having to create an extra JUnit class. - see `DemoProject/build.gradle`
###### Enhancements
- The `StartQFTestTask` now supports the property `fork` to decide, if the QF-Test process should be decoupled from the Gradle process (default) or not.
###### Bugfixes
- The plugin now supports execution with Java 1.8 again

#### 1.1.2
###### Bugfixes
- Multiple local QF-Test versions - the highest one is now selected correctly
###### Enhancements 
- Reworked project structure to have separate example and demo folders
- Demo project can be opened in IntelliJ IDEA and run directly tests
- Demo project has more examples in gradle.build and the test class 
- QF-Test argument "-verbose" passes output in Gradle console - suitable for CI/CD Integration - see DemoProject/build.gradle 'testLogging'

#### 1.1.1

##### Bugfixes
- Automatic detection of dev version number
- Removed implicit dependency from "java" plugin
- Evaluates the `--project-cache-dir` gradle argument

#### 1.1.0
##### New Features
- _StartQFTestTask_ added.
##### Bugfixes
- Automatically extract QF-Test on Windows without asking back when overriding shared files in the cacheDir.

### 1.0.0
- Initial Release

### License

This plugin source is available under the terms of the MIT License (MIT), as described in `LICENSE`