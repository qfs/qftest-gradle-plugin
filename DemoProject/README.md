## How to Integrate QF-Test Into Your Project
Add the plugin to your project's `build.gradle` using:

```groovy
 plugins {
 	id 'de.qfs.qftest' version '2.1.0'
 }
```

## How to run QF-Test suites during "test"
It is possible to include QF-Test test suites into the Junit 5 test run, integrating the results from the QF-Test test run into the
JUnit test results. To configure the test run, use the optional configuration parameters described at the end of this file.

### Running suites from test task
To run test suites during the test phase, reference them in the test task configuration as shown below.
If the input is a directory rather than a specific suite, all suites within that directory and any subdirectories will
be executed. The path can also be specified relatively to the `projectDir` as long as the directory or the suite files are located
there. You can use globs to make the path more flexible, e.g. `suites/*.qft` (searches the `suites` directory for all `.qft` files) or
`**/*.qft` (searches all subdirectories recursively for all `.qft` files). All `fileTree` syntax is supported. If only one
suite or directory should be executed, the brackets can be left out.

````groovy
test {
    useJUnitPlatform() // Junit 5 is required to execute the tests
    qftest {
        suites = ["$projectDir/suites/DemoTest.qft","C:/example/yourTestFolder/*.qft","..."]
        withReportOpen = true // Open the HTML report after running the test suites
    }
}
````

### Running suites from individual test class
Alternatively, QF-Test suite execution can be triggered from any JUnit 5 class.
To do so, extend the test class, which should include the execution of one or several QF-Test test suites, with a method
annotated with `de.qfs.apps.qftest.junit5.QFTest.Test`. The method must return an object of the type
`de.qfs.apps.qftest.junit5.QFTest`, which is created using the static method `QFTest.runSuite(...)`
or `QFTest.runSuites(...)`. 

```java
import de.qfs.apps.qftest.junit5.QFTest;
import java.io.File;

public class QFTestDemoTest
{
    @QFTest.Test
    QFTest demoTest() throws Exception {
        // Get location of demo testsuite
        final File qftestVerdir = QFTest.getVersionDir();
        final File demo = new File(qftestVerdir,"demo/carconfig/carconfig_en.qft");

        return QFTest.runSuite(demo)
                .withVariable("buggyMode","True")
                .withArgument("-verbose")
                .withReportOpen();
    }
}
```

If required, this object can be further configured e.g. to include QF-Test options or variables.
The provided methods are documented in the file `doc/javadoc/qftest-junit5.zip`
inside the QF-Test installation.


## Additional Tasks

### startQFTest
To simplify test development, you can run the included task `startQFTest` to easily open QF-Test in interactive mode:

```bash
./gradlew startQFTest
```

To add additional arguments to the process start, you can add your own tasks of the type _StartQFTestTask_ and define
your arguments there:

````groovy
task startQFTestWithDebugMenu(type: StartQFTestTask) {
    arguments = ["-debug", "-dbg"]
    // per default (fork = true), the QF-Test instance is started independently of the
    // Gradle process. To see the debug output of QF-Test in the console, we have to
    // couple the processes.
    fork = false
}
````

For the _StartQFTestTask_, only the _versionDir_, _version_, _baseUrl_, and _cacheDir_ parameters from the _qftest_ block are evaluated.
All other arguments must be directly supplied as described before.

### Optional gradle.build configuration
You can use the block `qftest` in the `build.gradle` file to set different parameters for downloading or integrating qftest into your project.

| Parameter     | Description	                                                                                                                                                                  | Default Value  |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| `versionDir`  | Set a specific versionDir with the path of your local QF-Test.                                                                                                                | empty          |
| `version`     | Set a specific version for the download. Set to `latest` for the latest stable QF-Test version. Set to `local` to use the highest version locally installed on the system.    | `"latest"`     |
| `baseUrl`     | Modify the URL where QF-Test is downloaded from. Usually you don't need to change this.                                                                                       | _QFS-Homepage_ |
| `cacheDir`    | Set a specific directory where QF-Test is downloaded to.                                                                                                                      | `.gradle`      |
| `license`     | Specify the license to use for the test runs.                                                                                                                                 | empty          |
| `runLogDir`   | Specify the directory where created QF-Test run logs will be stored, or the run log file directly.                                                                            | empty          |
| `keepRunLogs` | Keep all run logs, even successful ones (as boolean).                                                                                                                         | empty          |
| `reportDir`   | Enables report generation after the tests have been executed. The report will be generated for all test suites and run logs selected for this test run. (since QF-Test 7.1.2) | empty          |
| `reportArgs`  | Additional arguments to use for report generation  (as List).                                                                                                                 | empty          |
| `arguments`   | Pass arguments to QF-Test, started during test run like `["-dbg"]` (as List).                                                                                                 | empty          |
| `options`     | Pass options to QF-Test, started during test run (as Map).                                                                                                                    | empty          |
| `variables`   | Pass variables to QF-Test, started during test run (as Map).                                                                                                                  | empty          |
| `pluginClass` | If set to a string with the full qualified name of a class extending `de.qfs.apps.qftest.shared.QFTestPlugin`, supports developing QF-Test plugins. (since QF-Test 8.0.0)     | empty          |
| `devScopes`   | A list of strings (e.g. `"qftest"` or `"sut"`) defining the API-scopes required as ìmplementation` dependencies (Effectively the QF-Test jars added to the classpath).        | empty          |


#### Plugin configuration (Example)

In this example you can see the usage of all plugin parameters for test execution.
All parameters are optional, and some even exclude each other.

```groovy
 qftest {
    versionDir = "C:/Program Files/QFS/QF-Test/qftest-8.0.0"
    version = "latest" // not used, if "versionDir" is specified
    baseUrl = "http://qftest-repo.mycompany.com/stable" // not used, if "versionDir" is specified
    cacheDir = "C:/example/cacheDir"
    license = "C:/example/license.dat"
    runLogDir "C:/example/runLogDir"
    keepRunLogs = true
    reportDir = "C:/example/reportDir"
    reportArgs = ["-report-name","QF-Test Run %y%M%d"]
    arguments = ["-allow-shutdown", "-dbg"]
    options = ["OPT_LOG_CLIENT_PERFORMANCE":"true"]
    variables = ["longrun": "true"]
 }
```