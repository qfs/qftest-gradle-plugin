// Copyright (c) 2023 Quality First Software GmbH
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.

package de.qfs.examples;

import de.qfs.apps.qftest.junit5.QFTest;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class QFTestDemoTest {

    @QFTest.Test
    QFTest demoTest() throws Exception {
        // Get location of demo testsuite
        final File qftestVerdir = QFTest.getVersionDir();

        // Use Swing demo test
        File demo =  new File(qftestVerdir,"demo/carconfigSwing/carconfigSwing_en.qft");
        if (!demo.exists()) {
            demo =  new File(qftestVerdir,"demo/carconfig/carconfig_en.qft"); // Pre QF-Test 7.1
        }

        return QFTest.runSuite(demo).withArguments("-J-Duser.language=EN")
                .withReport();
        /*
        String tmpDir = "java.io.tmpdir";
            return QFTest.runSuite(demo).withArguments("-J-Duser.language=EN")
                .withVariable("buggyMode","True")
                .withRunLogPath(tmpDir + "/test/")
                .withReport(tmpDir + "/test/report/")
                .withReportOpen();*/
    }
}
